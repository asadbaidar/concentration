//
//  Card.swift
//  Concentration
//
//  Created by Asad Baidar on 16/04/2018.
//  Copyright © 2018 Codexoft Inc. All rights reserved.
//

import Foundation

struct Card: Hashable {
    
    var hashValue: Int {
        return id
    }
    
    static func == (lhs: Card, rhs: Card) -> Bool {
        return lhs.id == rhs.id
    }
    
    private var id: Int
    var isFaceUp = false
    var isMatched = false

    init() {
        id = Card.getUniqueId()
    }
    
    private static var uniqueId = 0
    
    private static func getUniqueId() -> Int {
        uniqueId += 1
        return uniqueId
    }
    
    static func resetUniqueId(){
        uniqueId  = 0
    }
}
