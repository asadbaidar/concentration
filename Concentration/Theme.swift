//
//  Theme.swift
//  Concentration
//
//  Created by Asad Baidar on 25/04/2018.
//  Copyright © 2018 Codexoft Inc. All rights reserved.
//

import Foundation
import UIKit

enum Theme {
    case none, halloween, animals, flags, faces
    
    var emoji: String { switch self {
        case .halloween: return "👻🎃💀😈👽🤡🧜‍♀️🧜‍♂️🧚‍♀️🧚‍♂️🧞‍♀️🧞‍♂️🧟‍♀️🧟‍♂️🧛‍♀️🧛‍♂️👮‍♀️💂‍♀️👨‍🍳👩‍🎓👑🕶👹👺☠️🤖🧙‍♀️🧙‍♂️🧝‍♀️🧝‍♂️💩👿👀👩‍🔬👨‍🎨👩‍🚀👨‍🏭👩‍🎨👨‍🔬👩‍⚖️🕴👸🤴🤵👨‍🚒🕵️‍♂️👩‍🎤👷‍♂️👲"
        case .animals: return "🐶🐱🐭🐹🐰🦊🐻🐼🐨🐯🦁🐮🐸🐵🐒🐔🐧🐤🐣🐥🦆🦅🦉🦇🐺🐗🐴🦄🐝🦋🐌🐞🐜🕷🦂🐢🐍🦖🦕🐙🦐🦀🐡🐠🐟🐬🐅🐆🐘🦏🦍🦓🐪🦒🐃🐂🐄🐎🐑🐐🦌🐓🐈🕊🐇🐿"
        case .flags: return "🇦🇫🇦🇽🇦🇱🇦🇴🇦🇮🇦🇲🇦🇼🇦🇺🇦🇿🇧🇧🇧🇾🇧🇿🇧🇯🇧🇮🇰🇭🇨🇲🇨🇦🇮🇨🇨🇻🇧🇶🇰🇾🇨🇫🇰🇲🇨🇬🇨🇰🇨🇷🇨🇮🇭🇷🇨🇺🇨🇼🇨🇾🇨🇿🇩🇰🇩🇯🇩🇲🇩🇴🇪🇨🇪🇬🇸🇻🇬🇶🇪🇷🇪🇪🇪🇹🇪🇺🇫🇰🇫🇴🇫🇮🇫🇷🇬🇫🇹🇫🇬🇦🇬🇲🇬🇪🇩🇪🇬🇭🇬🇷🇬🇱🇬🇩🇬🇵🇺🇸🇬🇧🇹🇷🇸🇦🇵🇰🇳🇪🇳🇿🇰🇷🇱🇰"
        case .faces: return "😀😃😄😁😆😅😂🤣☺️😊😇🙂🙃😉😌😍😘😗😙😚😋😛😝😜🤪🤨🧐🤓😎🤩😏😒😞😔😟🙁☹️😣😖😫😩😢😭😤😠😡🤯😳😱😥🤗🤔🤭🤫😶😬🙄😦😯😧😑😮😲😴🤤😵🤐🤧😷🤒🤑🤕"
        default: return Theme.halloween.emoji + Theme.animals.emoji + Theme.flags.emoji + Theme.faces.emoji
        }
    }
    
    var color: UIColor { switch self {
        case .halloween: return #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
        case .animals: return #colorLiteral(red: 0, green: 0.5628422499, blue: 0.3188166618, alpha: 1)
        case .flags: return #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        case .faces: return #colorLiteral(red: 0.5818830132, green: 0.2156915367, blue: 1, alpha: 1)
        default: return #colorLiteral(red: 0.5808190107, green: 0.0884276256, blue: 0.3186392188, alpha: 1)
        }
    }
    
    static func getFor(_ name: String) -> Theme {
        switch name {
        case "Halloween": return .halloween
        case "Animals": return .animals
        case "Flags": return .flags
        case "Faces": return .faces
        default: return .none
        }
    }
}
