//
//  Concentration.swift
//  Concentration
//
//  Created by Asad Baidar on 16/04/2018.
//  Copyright © 2018 Codexoft Inc. All rights reserved.
//

import Foundation

struct Concentration {
    
    private(set) var score = 0
    private(set) var flips = 0
    private(set) var cards = [Card]()
    var flipingIndices: [Int] = []
    
    init(cardPairsCount: Int) {
        Card.resetUniqueId()
        for _ in 1...cardPairsCount {
            let card = Card()
            cards += [card, card]
        }
        var cardsShufled = [Card]()
        for _ in 1...cardPairsCount {
            cardsShufled.append(cards.remove(at: cards.count.arc4random))
        }
        for _ in 1...cardPairsCount {
            cards.append(cardsShufled.remove(at: cardsShufled.count.arc4random))
        }
    }
    
    private var faceUpCardIndices: [Int] {
        return cards.indices.filter{ (cards[$0].isFaceUp && !cards[$0].isMatched)  || flipingIndices.contains($0) }
    }
    
    mutating func selectCard(at selectedIndex: Int) -> Status? {
        assert(cards.indices.contains(selectedIndex), "selectCard(at : \(selectedIndex)): index out of range")
        var status: Status? = nil
        if !cards[selectedIndex].isMatched, !cards[selectedIndex].isFaceUp {
            if let alreadyOneFaceUpIndex = faceUpCardIndices.onlyOne, alreadyOneFaceUpIndex != selectedIndex {
                flips += 1
                status = .selected
                if cards[alreadyOneFaceUpIndex] == cards[selectedIndex] {
                    status = .matched([alreadyOneFaceUpIndex, selectedIndex])
                    score += 5
                } else {
                    status = .not_matched([alreadyOneFaceUpIndex, selectedIndex])
                    if flips > cards.count { score -= 2 }
                }
            } else if faceUpCardIndices.isEmpty {
                flips += 1
                status = .selected
            }
        }
        return status
    }
    
    mutating func matchingCard(_ index: Int) {
        cards[index].isMatched = true
    }
    
    mutating func flipingCard(selected index: Int) {
        cards[index].isFaceUp = !cards[index].isFaceUp
    }
    
    enum Status {
        case matched([Int])
        case not_matched([Int])
        case selected
    }
}
