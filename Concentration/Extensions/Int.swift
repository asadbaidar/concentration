//
//  IntExtensions.swift
//  Concentration
//
//  Created by Asad Baidar on 20/04/2018.
//  Copyright © 2018 Codexoft Inc. All rights reserved.
//

import Foundation

extension Int {
    var arc4random: Int {
        if self == 0 {
            return 0
        } else {
            let random = Int(arc4random_uniform(UInt32(abs(self))))
            return self > 0 ? random : -random
        }
    }
}
