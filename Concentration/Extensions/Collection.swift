//
//  Collection.swift
//  Concentration
//
//  Created by Asad Baidar on 4/22/18.
//  Copyright © 2018 Codexoft Inc. All rights reserved.
//

import Foundation

extension Collection {
    
    var onlyOne: Element? {
        return count == 1 ? first : nil
    }
}
