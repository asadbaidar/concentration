//
//  ViewController.swift
//  Concentration
//
//  Created by Asad Baidar on 11/04/2018.
//  Copyright © 2018 Codexoft Inc. All rights reserved.
//

import UIKit

class ConcentrationController: UIViewController {
    
    private lazy var textAttributes : [NSAttributedStringKey : Any] = [
        .strokeColor : theme.color,
        .strokeWidth : 0.0
    ]
    
    @IBOutlet private weak var label_score: UILabel! {
        didSet {
            updateScore(score: 0)
        }
    }
    
    private func updateScore(score: Int) {
        label_score.attributedText = NSAttributedString(string: "Score \(score)", attributes: textAttributes)
    }
    
    @IBOutlet private weak var label_flips: UILabel! {
        didSet {
            updateFlips(flips: 0)
        }
    }
    
    private func updateFlips(flips: Int) {
        label_flips.attributedText = NSAttributedString(string: "Flips \(flips)", attributes: textAttributes)
    }
    
    @IBOutlet private var button_cards: [UIButton]!
    
    private var cards: [UIButton]! {
        return button_cards?.filter{ !$0.superview!.isHidden }
    }
    
    @IBOutlet weak var button_restart: UIButton!
    
    @IBAction private func onClickCard(_ sender: UIButton) {
        flipCard(sender)
    }
    
    @IBAction private func onClickRestart(_ sender: UIButton) {
        reset()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateTheme()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateCards()
    }
    
    func reset() {
        game = concentration
        updateTheme()
        updateCards()
        updateScore(score: game.score)
        updateFlips(flips: game.flips)
    }
    
    private lazy var game = concentration
    
    private var concentration: Concentration {
        return Concentration(cardPairsCount: (cards.count + 1) / 2)
    }
    
    private func flipCard(_ sender: UIButton) {
        if let index = cards.index(of: sender), let status = game.selectCard(at: index) {
            game.flipingIndices.append(index)
            UIView.transition(
                with: sender,
                duration: 0.4,
                options: [.transitionFlipFromLeft],
                animations: {
                    self.game.flipingCard(selected: index)
                    self.updateView(sender, from: self.game.cards[index])
                },
                completion: { finished in
                    self.game.flipingIndices.remove(at: self.game.flipingIndices.index(of: index)!)
                    switch status {
                    case let .matched(indices):
                        UIViewPropertyAnimator.runningPropertyAnimator(
                            withDuration: 0.3,
                            delay: 0,
                            options: [],
                            animations: { indices.forEach { index in
                                self.game.matchingCard(index)
                                self.cards[index].transform = CGAffineTransform.identity.scaledBy(x: 1.6, y: 1.6) }
                            },
                            completion: { finished in
                                UIViewPropertyAnimator.runningPropertyAnimator(
                                    withDuration: 0.2,
                                    delay: 0,
                                    options: [],
                                    animations: { indices.forEach { index in
                                        self.cards[index].transform = CGAffineTransform.identity.scaledBy(x: 0.01, y: 0.01) }
                                    },
                                    completion: { finished in
                                        indices.forEach { index in
                                            self.updateView(self.cards[index], from: self.game.cards[index])
                                            self.cards[index].transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
                                        }
                                    }
                                )
                            }
                        )
                    case let .not_matched(indices): indices.forEach { index in
                        UIView.transition(with: self.cards[index],
                                          duration: 0.6,
                                          options: [.transitionFlipFromLeft],
                                          animations: {
                                            self.game.flipingCard(selected: index)
                                            self.updateView(self.cards[index], from: self.game.cards[index])
                                          })
                        }
                    case .selected: break
                    }
                })
        }
        updateScore(score: game.score)
        updateFlips(flips: game.flips)
    }
    
    private func updateCards() {
        if cards != nil {
            for index in cards.indices {
                updateView(cards[index], from: game.cards[index])
            }
        }
    }
    
    private func updateView(_ sender: UIButton, from card: Card) {
        if card.isMatched {
            sender.setTitle("", for: UIControlState.normal)
            sender.backgroundColor = #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 0)
            sender.isEnabled = false
        } else {
            sender.setTitle(card.isFaceUp ? getEmoji(for: card) : "", for: UIControlState.normal)
            sender.backgroundColor = card.isFaceUp ? #colorLiteral(red: 0.921431005, green: 0.9214526415, blue: 0.9214410186, alpha: 1) : theme.color
            sender.isEnabled = true
        }
    }
    
    var theme = Theme.halloween {
        didSet {
            updateTheme()
            updateCards()
        }
    }
    
    private func updateTheme() {
        emojiCardPairs.removeAll()
        emojis = Array(theme.emoji)
        button_restart?.setTitleColor(theme.color, for: UIControlState.normal)
        label_flips?.textColor = theme.color
        label_score?.textColor = theme.color
    }
    
    private lazy var emojis = Array(theme.emoji)
    private var emojiCardPairs = [Card : String]()
    
    private func getEmoji(for card: Card) -> String {
        if emojiCardPairs[card] == nil, emojis.count > 0 {
            emojiCardPairs[card] = String(emojis.remove(at: emojis.count.arc4random))
        }
        return emojiCardPairs[card] ?? "?"
    }
}

