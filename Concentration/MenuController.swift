//
//  MenuController.swift
//  Concentration
//
//  Created by Asad Baidar on 25/04/2018.
//  Copyright © 2018 Codexoft Inc. All rights reserved.
//

import UIKit

class MenuController: UIViewController, UISplitViewControllerDelegate {
    
    override func awakeFromNib() {
        splitViewController?.delegate = self
    }
    
    // MARK: - on start of split view controller, collapse secondary if true, else don't
    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
        return (secondaryViewController as? ConcentrationController)?.theme == .halloween
    }
    
    var concentrationFromSplitViewController: ConcentrationController? {
        if let viewControllers = splitViewController?.viewControllers {
            for viewController in viewControllers {
                if viewController is ConcentrationController {
                    return viewController as? ConcentrationController
                }
            }
        }
        return nil
    }
    
    // Strong Pointer to keep it for next theme without being reset
    var concentrationFromLastSegued: ConcentrationController?
    
    @IBAction func onMenuItemSelected(_ sender: Any) {
        if let concentration = concentrationFromSplitViewController {
            changeTheme(of: concentration, to: sender)
        } else if let concentration = concentrationFromLastSegued {
            changeTheme(of: concentration, to: sender)
            navigationController?.pushViewController(concentration, animated: true)
        } else {
            performSegue(withIdentifier: "play", sender: sender)
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "play" {
            changeTheme(of: segue.destination, to: sender)
        }
    }
    
    func changeTheme(of destination: Any?, to sender: Any?) {
        if let name = (sender as? UIButton)?.currentTitle, let concentration = destination as? ConcentrationController {
            concentration.theme = Theme.getFor(name)
            concentrationFromLastSegued = concentration
        }
    }
}
